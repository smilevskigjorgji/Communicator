package com.example.communicatorapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ChangeInfoActivity extends AppCompatActivity {

    EditText editTextIme, editTextAdresa, editTextTel;
    Button buttonProdolzi, buttonSlika, buttonReset;
    RadioButton buttonMashko, buttonZensko;
    ImageView imageSlika;
    SharedPreferences pref;
    String encodedImage;
    private static int RESULT_LOAD_IMG = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        editTextIme = findViewById(R.id.editTextIme);
        editTextAdresa = findViewById(R.id.editTextAdresa);
        editTextTel = findViewById(R.id.editTextTel);
        buttonMashko = findViewById(R.id.button_mashko);
        buttonZensko = findViewById(R.id.button_zensko);
        buttonProdolzi = findViewById(R.id.button_prodolzi);
        buttonReset = findViewById(R.id.button_reset);
        buttonSlika = findViewById(R.id.button_slika);
        imageSlika = findViewById(R.id.image_slika);

        pref = getSharedPreferences("ActivityPREF", Context.MODE_PRIVATE);

        String ime = pref.getString("ime", null);
        String adresa = pref.getString("adresa", null);
        String telefon = pref.getString("telefon", null);
        encodedImage = pref.getString("slika", null);
        String pol = pref.getString("pol", null);
        if(encodedImage!=null){
            byte[] b = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            imageSlika.setImageBitmap(bitmap);
        }
        editTextIme.setText(ime);
        editTextAdresa.setText(adresa);
        editTextTel.setText(telefon);
        if(pol == "Машко"){
            buttonMashko.setChecked(true);
            buttonZensko.setChecked(false);
        }
        else if(pol == "Женско"){
            buttonMashko.setChecked(false);
            buttonZensko.setChecked(true);
        }

        buttonSlika.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
            }
        });

        buttonProdolzi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor ed = pref.edit();
                ed.putString("ime", editTextIme.getText().toString());
                ed.putString("adresa", editTextAdresa.getText().toString());
                ed.putString("telefon", editTextTel.getText().toString());
                ed.putString("slika", encodedImage);
                if(buttonMashko.isChecked()==true){
                    ed.putString("pol", "Машко");
                }
                else if(buttonZensko.isChecked()==true){
                    ed.putString("pol", "Женско");
                }
                ed.commit();
                Intent intent = new Intent(ChangeInfoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        buttonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageSlika.setImageBitmap(null);
                encodedImage = null;
                editTextIme.setText("");
                editTextAdresa.setText("");
                editTextTel.setText("");
                buttonMashko.setChecked(true);
                buttonZensko.setChecked(false);
            }
        });

    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                Bitmap image = rotateBitmap(this, imageUri, selectedImage);
                imageSlika.setImageBitmap(image);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] b = baos.toByteArray();
                encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(ChangeInfoActivity.this, "Грешка", Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(ChangeInfoActivity.this, "Одберете слика",Toast.LENGTH_LONG).show();
        }
    }

    private static int getOrientation(Context context, Uri photoUri) {
        Cursor cursor = context.getContentResolver().query(photoUri,
                new String[]{MediaStore.Images.ImageColumns.ORIENTATION}, null, null, null);
        if (cursor.getCount() != 1) {
            cursor.close();
            return -1;
        }
        cursor.moveToFirst();
        int orientation = cursor.getInt(0);
        cursor.close();
        cursor = null;
        return orientation;
    }

    public static Bitmap rotateBitmap(Context context, Uri photoUri, Bitmap bitmap) {
        int orientation = getOrientation(context, photoUri);
        if (orientation <= 0) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        matrix.postRotate(orientation);
        bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        return bitmap;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ChangeInfoActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
